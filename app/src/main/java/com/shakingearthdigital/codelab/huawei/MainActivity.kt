package com.shakingearthdigital.codelab.huawei

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.huawei.hms.maps.HuaweiMap
import com.huawei.hms.maps.MapView
import com.huawei.hms.maps.OnMapReadyCallback
import com.huawei.hms.maps.model.LatLng
import com.huawei.hms.maps.model.Polyline

import com.huawei.hms.maps.model.PolylineOptions


class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private val mPolylinePoints: MutableList<LatLng> = arrayListOf()
    private val TAG = "MapViewDemoActivity"

    //HUAWEI map
    private var hMap: HuaweiMap? = null

    private var mMapView: MapView? = null

    private val MAPVIEW_BUNDLE_KEY = "MapViewBundleKey"

    private val REQUEST_CODE = 747

    private val RUNTIME_PERMISSIONS = arrayOf<String>(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET
    )

    private var mPolyline: Polyline? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!hasPermissions(this, *RUNTIME_PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, RUNTIME_PERMISSIONS, REQUEST_CODE)
        }
        // get mapview instance
        mMapView = findViewById(R.id.mapView)
        val mapViewBundle: Bundle? = savedInstanceState?.getBundle(MAPVIEW_BUNDLE_KEY)
        mMapView?.onCreate(mapViewBundle)
        mMapView?.getMapAsync(this)
    }

    override fun onMapReady(map: HuaweiMap?) {
        Log.d(TAG, "onMapReady")
        hMap = map;
        hMap?.isMyLocationEnabled = true;// Enable the my-location overlay.
        hMap?.uiSettings?.isMyLocationButtonEnabled = true;// Enable the my-location icon.

        //add polyline which draws whenever you move the camera
        mPolylinePoints.add(hMap!!.cameraPosition!!.target)
        mPolyline = hMap?.addPolyline(
            PolylineOptions()
                .addAll(mPolylinePoints)
                .color(Color.BLUE)
                .width(3f)
        )

        hMap?.setOnCameraMoveListener {
            mPolylinePoints.add(hMap!!.cameraPosition!!.target)
            mPolyline?.points = mPolylinePoints
        }
    }

    override fun onStart() {
        super.onStart()
        mMapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mMapView?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView?.onDestroy()
    }

    override fun onPause() {
        mMapView?.onPause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        mMapView?.onResume()
    }

    private fun hasPermissions(
        context: Context,
        vararg permissions: String
    ): Boolean {
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }
}