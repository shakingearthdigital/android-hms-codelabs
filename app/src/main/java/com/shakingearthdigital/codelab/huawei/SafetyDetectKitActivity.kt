package com.shakingearthdigital.codelab.huawei

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import com.huawei.hms.common.ApiException
import com.huawei.hms.support.api.entity.core.CommonCode
import com.huawei.hms.support.api.entity.safetydetect.MaliciousAppsData
import com.huawei.hms.support.api.safetydetect.SafetyDetect
import com.huawei.hms.support.api.safetydetect.SafetyDetectClient
import com.huawei.hms.support.api.safetydetect.SafetyDetectStatusCodes
import kotlinx.android.synthetic.main.activity_safety_detect_kit.*

class SafetyDetectKitActivity : AppCompatActivity() {
    private val TAG = "SafetyDetectKitActivity"

    private lateinit var client: SafetyDetectClient

    /**
     * URLs of this type are marked as URLs of pages containing potentially malicious apps (such as home page tampering URLs, Trojan-infected URLs, and malicious app download URLs).
     */
    val MALWARE = 1

    /**
     * URLs of this type are marked as phishing and spoofing URLs.
     */
    val PHISHING = 3


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        client = SafetyDetect.getClient(this@SafetyDetectKitActivity)
        client.initUrlCheck()

        setContentView(R.layout.activity_safety_detect_kit)

        btnMaliciousApps.setOnClickListener { getMaliciousApps() }
        btnCheckURL.setOnClickListener { checkURL() }
    }

    private fun getMaliciousApps() {
        SafetyDetect.getClient(this@SafetyDetectKitActivity)
            .maliciousAppsList
            .addOnSuccessListener { maliciousAppsListResp ->
                val appsDataList: List<MaliciousAppsData> = maliciousAppsListResp.maliciousAppsList
                if (maliciousAppsListResp.rtnCode == CommonCode.OK) {
                    if (appsDataList.isEmpty()) {
                        Toast.makeText(applicationContext, "No threat", Toast.LENGTH_SHORT).show()
                    } else {
                        for (maliciousApp in appsDataList) {
                            Log.i(TAG, "Information about a malicious app:")
                            Log.i(TAG, "  APK: ${maliciousApp.apkPackageName}")
                            Log.i(TAG, "  SHA-256: ${maliciousApp.apkSha256}")
                            Log.i(TAG, "  Category: ${maliciousApp.apkCategory}")
                        }
                        // could show malicious apps on screen here
                    }
                } else {
                    Log.e(TAG, "Failed to get malicious apps : ${maliciousAppsListResp.errorReason}")
                    Toast.makeText(applicationContext, "Failed to get malicious apps : ${maliciousAppsListResp.errorReason}", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener { apiException -> // There was an error communicating with the service.
                val errorMsg: String? = if (apiException is ApiException) {
                    // An error with the HMS API contains some additional details and You can use the apiException.getStatusCode() method to get the status code.
                    "${SafetyDetectStatusCodes.getStatusCodeString(apiException.statusCode)} : ${apiException.message}"
                    // .
                } else {
                    // Unknown type of error has occurred.
                    apiException.message
                }
                Log.e(TAG, "Failed to get malicious apps $errorMsg")
                Toast.makeText(applicationContext, "Failed to get malicious apps : $errorMsg", Toast.LENGTH_SHORT).show()
            }
    }

    @SuppressLint("SetTextI18n")
    private fun checkURL(){
        val realUrl = editTextURL.text.toString().trim()
// Specify url threat type
        client.urlCheck(realUrl, "102412269", MALWARE, PHISHING)
            .addOnSuccessListener { urlCheckResponse ->
                /**
                 * Called after successfully communicating with the SafetyDetect API.
                 * The #onSuccess callback receives an
                 * [com.huawei.hms.support.api.entity.safetydetect.UrlCheckResponse] that contains a
                 * list of UrlCheckThreat that contains the threat type of the Url.
                 */
                // Indicates communication with the service was successful.
                // Identify any detected threats.
                // Call getUrlCheckResponse method of UrlCheckResponse then you can get List<UrlCheckThreat> .
                // If List<UrlCheckThreat> is empty , that means no threats found , else that means threats found.
                val list = urlCheckResponse.urlCheckResponse
                if (list.isEmpty()) {
                    // No threats found.
                    textViewUrlResult.text = "No threat found"
                } else {
                    // Threats found!
                    textViewUrlResult.text = "Threats found"
                }
            }
            .addOnFailureListener { apiException ->
                /**
                 * Called when an error occurred when communicating with the SafetyDetect API.
                 */
                // An error with the Huawei Mobile Service API contains some additional details and You can use the apiException.getStatusCode() method to get the status code.
                val errorMsg: String? = if (apiException is ApiException) {
                    "Error : ${SafetyDetectStatusCodes.getStatusCodeString(apiException.statusCode)} : ${apiException.message}"
                    // Note: If the status code is SafetyDetectStatusCodes.CHECK_WITHOUT_INIT, you need to call initUrlCheck().
                } else {
                    // Unknown type of error has occurred.
                    apiException.message
                }
                Log.d(TAG, "URL Check Failed $errorMsg")
                textViewUrlResult.text = "URL Check $errorMsg"
            }

    }

}