# SED Huawei HMS Codelabs

## Maps codelab
https://developer.huawei.com/consumer/en/codelab/HMSMapKit/index.html#0  
**MainActivity**  
Also draws a polyline whenever you move the map, just for fun

## Account codelab
https://developer.huawei.com/consumer/en/codelab/HMSAccounts/index.html#0  
**AccountActivity**  

## Site kit codelab
https://developer.huawei.com/consumer/en/codelab/HMSSiteKit-Kotlin/index.html#0  
**SiteKitActivity**  

## Drive kit codelab
https://developer.huawei.com/consumer/en/codelab/HMSDriveKit/index.html#3  
**DriveKitActivity**  
*Issues with login*  

## Safety Detect kit codelab
https://developer.huawei.com/consumer/en/codelab/HMSCoreSafetyDetect/index.html#0  
**SafetyDetectKitActivity**  

## ML kit - Image segmentation
https://developer.huawei.com/consumer/en/doc/development/HMSCore-Guides/image-segmentation-0000001050040109  
LensEngine - https://developer.huawei.com/consumer/en/doc/HMSCore-References/lensengine-0000001050169367-V5  
**ImageSegmentationActivity**  
*We have outstanding questions about syncing camera preview to segmented image, & issues with result images*  

## Audio kit codelab
https://developer.huawei.com/consumer/en/codelab/HMSAudioKit/index.html#0
**AudioKitActivity**
