package com.shakingearthdigital.codelab.huawei;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.SparseArray;
import android.view.TextureView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.huawei.hms.mlsdk.MLAnalyzerFactory;
import com.huawei.hms.mlsdk.common.LensEngine;
import com.huawei.hms.mlsdk.common.MLAnalyzer;
import com.huawei.hms.mlsdk.imgseg.MLImageSegmentation;
import com.huawei.hms.mlsdk.imgseg.MLImageSegmentationAnalyzer;
import com.huawei.hms.mlsdk.imgseg.MLImageSegmentationScene;
import com.huawei.hms.mlsdk.imgseg.MLImageSegmentationSetting;

import java.io.IOException;
import java.util.ArrayList;

public class ImageSegmentationActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener {

    private static final int CAMERA_PERMISSION_CODE = 723;
    private static final String TAG = ImageSegmentationActivity.class.getSimpleName();
    private MLImageSegmentationAnalyzer analyzer;
    private LensEngine mLensEngine;
    private TextureView mPreview;
    private ImageView mSegmentationFg;
    private ImageView mSegmentationBg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Body Segmentation");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            setupView();
        } else {
            this.requestCameraPermission();
        }
    }

    private void setupView() {
        Log.d(TAG, "setupView");
        createImageTransactor();
        createLensEngine();

        setContentView(R.layout.activity_image_segmentation);
        mSegmentationFg = findViewById(R.id.imageSegmentationOverlay);
        mSegmentationBg = findViewById(R.id.imageSegmentationBg);
        mPreview = findViewById(R.id.imageSegmentationPreview);
        mPreview.setSurfaceTextureListener(this);

        setFgText((TextView)findViewById(R.id.textViewFg));
    }

    private void setFgText(TextView textView) {
        CharSequence text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum";
        Spannable spannable = new SpannableStringBuilder(text);

        char space = ' ';
        ArrayList<Integer> spaces = new ArrayList<>();
        // TODO make every other word transparent
        for(int i = 0; i < text.length(); i++){
            if (space == text.charAt(i)){
//                Log.d(TAG, "found space at "+i);
                spaces.add(i);
            }
        }

        int toggle = 0;
        for (int i = 0; i < spaces.size()-1; i++){
            if ((toggle++ % 2) == 0 ) {
                spannable.setSpan(new ForegroundColorSpan(Color.TRANSPARENT), spaces.get(i), spaces.get(i + 1), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        textView.setText(spannable);


    }

    @Override
    protected void onPause() {
        super.onPause();
//        mLensEngine.close();
//        mLensEngine.release();
    }

    private void requestCameraPermission() {
        Log.d(TAG, "requestCameraPermission");
        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, CAMERA_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != CAMERA_PERMISSION_CODE) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }
        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            setupView();
        }
    }

    private void createImageTransactor() {
        Log.d(TAG, "createImageTransactor");
        MLImageSegmentationSetting setting = new MLImageSegmentationSetting.Factory()
                .setAnalyzerType(MLImageSegmentationSetting.BODY_SEG)
                .setScene(MLImageSegmentationScene.ALL)
                .setExact(false)
                .create();
        this.analyzer = MLAnalyzerFactory.getInstance().getImageSegmentationAnalyzer(setting);
        analyzer.setTransactor(new ImageSegmentAnalyzerTransactor());
    }

    private  void createLensEngine(){
        Log.d(TAG, "createLensEngine");
        Context context = this.getApplicationContext();
        mLensEngine = new LensEngine.Creator(context,analyzer)
                // Set the front or rear camera mode. LensEngine.BACK_LENS indicates the rear camera, and LensEngine.FRONT_LENS indicates the front camera.
                .setLensType(LensEngine.FRONT_LENS)
                .applyDisplayDimension(1280, 720)
                .applyFps(20.0f)
                .enableAutomaticFocus(true)
                .create();
    }

    @Override
    public void onSurfaceTextureAvailable(@NonNull SurfaceTexture surfaceTexture, int width, int height) {
        Log.d(TAG, "onSurfaceTextureAvailable "+width+"x"+height);
        try {
            mLensEngine.run(surfaceTexture);
            float screenWidth = (float) getResources().getDisplayMetrics().widthPixels;
            float scale = screenWidth / width;
            mPreview.setPivotX(0);
            mPreview.setPivotY(0);
            mPreview.setScaleX(scale);
            mPreview.setScaleY(scale);

            mSegmentationBg.setPivotX(0);
            mSegmentationBg.setPivotY(0);
            mSegmentationBg.setScaleX(-scale);
            mSegmentationBg.setScaleY(scale);
            mSegmentationBg.setTranslationX(screenWidth);

            mSegmentationFg.setPivotX(0);
            mSegmentationFg.setPivotY(0);
            mSegmentationFg.setScaleX(-scale);
            mSegmentationFg.setScaleY(scale);
            mSegmentationFg.setTranslationX(screenWidth);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(@NonNull SurfaceTexture surfaceTexture, int width, int height) {
        Log.d(TAG, "onSurfaceTextureSizeChanged "+width+"x"+height);

    }

    @Override
    public boolean onSurfaceTextureDestroyed(@NonNull SurfaceTexture surfaceTexture) {
        mLensEngine.close();
        mLensEngine.release();
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(@NonNull SurfaceTexture surfaceTexture) {

    }

    public class ImageSegmentAnalyzerTransactor implements MLAnalyzer.MLTransactor<MLImageSegmentation> {
        @Override
        public void transactResult(MLAnalyzer.Result<MLImageSegmentation> results) {
            Log.d(TAG, "ImageSegmentAnalyzerTransactor : transactResult");
            SparseArray<MLImageSegmentation> items = results.getAnalyseList();
            for(int i = 0, arraySize= items.size(); i < arraySize; i++) {
                MLImageSegmentation img = items.valueAt(i);
//                Bitmap foreground = img.getForeground();
//            Log.d(TAG, "ImageSegmentAnalyzerTransactor : transactResult bitmap"+foreground.getWidth()+"x"+foreground.getHeight());
//                Size displayDimension = mLensEngine.getDisplayDimension();
//                Log.d(TAG, "ImageSegmentAnalyzerTransactor : transactResult Lens"+displayDimension.getWidth()+"x"+displayDimension.getHeight());
            mSegmentationFg.setImageBitmap(img.getForeground());
            mSegmentationBg.setImageBitmap(img.getOriginal());

            }


            // Determine detection result processing as required. Note that only the detection results are processed.
            // Other detection-related APIs provided by ML Kit cannot be called.
        }
        @Override
        public void destroy() {
            // Callback method used to release resources when the detection ends.
        }
    }
}