package com.shakingearthdigital.codelab.huawei

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.huawei.hms.site.api.SearchResultListener
import com.huawei.hms.site.api.SearchService
import com.huawei.hms.site.api.SearchServiceFactory
import com.huawei.hms.site.api.model.*
import kotlinx.android.synthetic.main.activity_site_kit.*
import java.io.UnsupportedEncodingException
import java.net.URLEncoder


class SiteKitActivity : AppCompatActivity() {

    private val TAG = "SiteKitActivity"

    private var searchService : SearchService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_site_kit)

        try {
            searchService = SearchServiceFactory.create(this, URLEncoder.encode("CV8Q5YPXdbEd5XlR2M8HoashQe94y/hlGRes4uH+L++6OOwNEkLOHkC8zL/PbHgcQ5affkFiwXSV4H0cIfOXcmE7CDPl", "utf-8"))
        } catch (e: UnsupportedEncodingException){
            Log.e(TAG, "encode apikey error")
        }

    }

    fun search(view : View){
        val textSearchRequest = TextSearchRequest()
        textSearchRequest.query = edit_text_text_search_query.text.toString()
        Log.d(TAG, "query="+textSearchRequest.query)
        textSearchRequest.hwPoiType = HwLocationType.TOWER
        searchService!!.textSearch(
            textSearchRequest,
            object : SearchResultListener<TextSearchResponse> {
                override fun onSearchResult(textSearchResponse: TextSearchResponse) {
                    val response = StringBuilder("\n")
                    response.append("success\n")
                    var count = 1
                    var addressDetail: AddressDetail
                    for (site in textSearchResponse.sites) {
                        addressDetail = site.address
                        response.append(
                            String.format(
                                "[%s]  name: %s, formatAddress: %s, country: %s, countryCode: %s \r\n",
                                "" + count++, site.name, site.formatAddress,
                                if (addressDetail == null) "" else addressDetail.country,
                                if (addressDetail == null) "" else addressDetail.countryCode
                            )
                        )
                    }
                    Log.d(TAG, "search result is : $response")
                    response_text_search.text = response.toString()
                }

                override fun onSearchError(searchStatus: SearchStatus) {
                    Log.e(TAG, "onSearchError is: " + searchStatus.errorCode)
                }
            })
    }
}