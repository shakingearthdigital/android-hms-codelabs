package com.shakingearthdigital.codelab.huawei

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.huawei.hmf.tasks.Task
import com.huawei.hms.common.ApiException
import com.huawei.hms.support.hwid.HuaweiIdAuthManager
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParams
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParamsHelper
import com.huawei.hms.support.hwid.service.HuaweiIdAuthService
import kotlinx.android.synthetic.main.activity_account.*


class AccountActivity : AppCompatActivity() {

    private val TAG = "AccountActivity"
    private val REQUEST_CODE = 748

    private var authService : HuaweiIdAuthService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)

        btnIDToken.setOnClickListener {
            val mHuaweiIdAuthParams: HuaweiIdAuthParams =
                HuaweiIdAuthParamsHelper(HuaweiIdAuthParams.DEFAULT_AUTH_REQUEST_PARAM).setIdToken()
                    .createParams()
            authService =
                HuaweiIdAuthManager.getService(this@AccountActivity, mHuaweiIdAuthParams)
            startActivityForResult(authService?.signInIntent, REQUEST_CODE)
        }

        btnAuthCode.setOnClickListener {
            val authParams =
                HuaweiIdAuthParamsHelper(HuaweiIdAuthParams.DEFAULT_AUTH_REQUEST_PARAM).setAuthorizationCode()
                    .createParams()
            authService =
                HuaweiIdAuthManager.getService(this@AccountActivity, authParams)
            startActivityForResult(authService?.getSignInIntent(), REQUEST_CODE)
        }

        btnSignout.setOnClickListener{
            // Use the HuaweiIdAuthService instance to call the getService API. The service is generated during authorization.
            if (authService != null) {
                val signOutTask: Task<Void> = authService!!.signOut()
                signOutTask.addOnCompleteListener {
                    logView.text = "Logout complete"
                }
            } else {
                logView.text = "AuthService is null. You need to log in first"
            }

        }
    }


    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            //login success
            //get user message by parseAuthResultFromIntent
            val authHuaweiIdTask =
                HuaweiIdAuthManager.parseAuthResultFromIntent(data)
            if (authHuaweiIdTask.isSuccessful) {
                val huaweiAccount = authHuaweiIdTask.result
                Log.i(TAG, "signIn success " + huaweiAccount.displayName)
                logView.text = "signIn success " + huaweiAccount.displayName
            } else {
                Log.i(
                    TAG, "signIn failed: " + (authHuaweiIdTask.exception as ApiException).statusCode
                )
                logView.text = "signIn failed: " + (authHuaweiIdTask.exception as ApiException).statusCode
            }
        }
    }

}