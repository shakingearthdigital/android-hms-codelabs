package com.shakingearthdigital.codelab.huawei

import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.huawei.cloud.base.auth.DriveCredential
import com.huawei.cloud.base.auth.DriveCredential.AccessMethod
import com.huawei.cloud.base.util.StringUtils
import com.huawei.cloud.client.exception.DriveCode
import com.huawei.cloud.services.drive.DriveScopes
import com.huawei.hms.common.ApiException
import com.huawei.hms.support.api.entity.auth.Scope
import com.huawei.hms.support.hwid.HuaweiIdAuthAPIManager
import com.huawei.hms.support.hwid.HuaweiIdAuthManager
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParams.DEFAULT_AUTH_REQUEST_PARAM
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParamsHelper
import kotlinx.android.synthetic.main.activity_drive_kit.*


class DriveKitActivity : AppCompatActivity() {
    private val TAG = "DriveKitActivity"
    private val REQUEST_SIGN_IN_LOGIN = 2247

    private val PERMISSIONS_STORAGE = arrayOf<String>(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )

    private var accessToken : String? = null
    private var unionId : String? = null
    private var mCredential : DriveCredential? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drive_kit)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(PERMISSIONS_STORAGE, 1)
        }


        btnLogin.setOnClickListener { driveLogin() }
    }

    private fun driveLogin() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        val scopeList: MutableList<Scope> = ArrayList()
        scopeList.add(Scope(DriveScopes.SCOPE_DRIVE)) // All permissions, except permissions for the app folder.
        scopeList.add(Scope(DriveScopes.SCOPE_DRIVE_READONLY)) // Permissions to view file content and metadata.
        scopeList.add(Scope(DriveScopes.SCOPE_DRIVE_FILE)) // Permissions to view and manage files.
        scopeList.add(Scope(DriveScopes.SCOPE_DRIVE_METADATA)) // Permissions to view and manage file metadata, excluding file content.
        scopeList.add(Scope(DriveScopes.SCOPE_DRIVE_METADATA_READONLY)) // Permissions to view file metadata, excluding file content.
        scopeList.add(Scope(DriveScopes.SCOPE_DRIVE_APPDATA)) // Permissions to upload and store app data.
        scopeList.add(HuaweiIdAuthAPIManager.HUAWEIID_BASE_SCOPE) // Basic account permissions.
        val authParams = HuaweiIdAuthParamsHelper(DEFAULT_AUTH_REQUEST_PARAM)
            .setAccessToken()
            .setIdToken()
            .setScopeList(scopeList)
            .createParams()
        // Call the account API to get account information.
        val client = HuaweiIdAuthManager.getService(this, authParams)
        startActivityForResult(client.signInIntent, REQUEST_SIGN_IN_LOGIN)
    }

    private val refreshAT = AccessMethod {
        /**Simplified code snippet for demonstration purposes. For the complete code snippet, please go to Client Development > Obtaining Authentication Information > Store Authentication Information in the HUAWEI Drive Kit Development Guide. */
        accessToken!!
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i(TAG, "onActivityResult, requestCode = $requestCode, resultCode = $resultCode")
        if (requestCode == REQUEST_SIGN_IN_LOGIN) {
            val authHuaweiIdTask =
                HuaweiIdAuthManager.parseAuthResultFromIntent(data)
            if (authHuaweiIdTask.isSuccessful) {
                val huaweiAccount = authHuaweiIdTask.result
                accessToken = huaweiAccount.accessToken
                unionId = huaweiAccount.unionId
                val returnCode: Int = init(unionId!!, accessToken!!, refreshAT)
                if (DriveCode.SUCCESS == returnCode) {
                    Log.d(TAG,"login ok")
                } else if (DriveCode.SERVICE_URL_NOT_ENABLED == returnCode) {
                    Log.e(TAG,"drive is not enabled")
                } else {
                    Log.e(TAG,"login error")
                }
            } else {
                Log.d(
                    TAG,
                    "onActivityResult, signIn failed: " + (authHuaweiIdTask.exception as ApiException).statusCode
                )
                Toast.makeText(
                    applicationContext,
                    "onActivityResult, signIn failed.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    /**
     * Initialize Drive based on the context and HUAWEI ID information including unionId, countrycode, and accessToken.
     * When the current accessToken expires, register an AccessMethod and obtain a new accessToken.
     *
     * @param unionID   unionID from HwID
     * @param at        access token
     * @param refreshAT a callback to refresh AT
     */
    fun init(unionID: String?, at: String?, refreshAT: AccessMethod?): Int {
        if (StringUtils.isNullOrEmpty(unionID) || StringUtils.isNullOrEmpty(at)) {
            return DriveCode.ERROR
        }
        val builder = DriveCredential.Builder(unionID, refreshAT)
        mCredential = builder.build().setAccessToken(at)
        return DriveCode.SUCCESS
    }
}
