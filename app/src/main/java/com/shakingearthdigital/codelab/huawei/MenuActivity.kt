package com.shakingearthdigital.codelab.huawei

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        btnAccount.setOnClickListener { startActivity(Intent(this@MenuActivity, AccountActivity::class.java)) }
        btnMap.setOnClickListener { startActivity(Intent(this@MenuActivity, MainActivity::class.java)) }
        btnSite.setOnClickListener { startActivity(Intent(this@MenuActivity, SiteKitActivity::class.java)) }
        btnDrive.setOnClickListener { startActivity(Intent(this@MenuActivity, DriveKitActivity::class.java)) }
        btnSafetyDetect.setOnClickListener { startActivity(Intent(this@MenuActivity, SafetyDetectKitActivity::class.java)) }
        btnImageSegmentation.setOnClickListener { startActivity(Intent(this@MenuActivity, ImageSegmentationActivity::class.java)) }
        btnAudioKit.setOnClickListener { startActivity(Intent(this@MenuActivity, AudioKitActivity::class.java)) }
    }
}