package com.shakingearthdigital.codelab.huawei;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.pm.PackageInfoCompat;

public class TestActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        // Set version text
        PackageManager manager = getApplicationContext().getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(getApplicationContext().getPackageName(), 0);
                info.getLongVersionCode();
                ((TextView)findViewById(R.id.versionTextView)).setText(getString(R.string.versionLabel,  info.versionName));
        } catch (PackageManager.NameNotFoundException e) {
            // hide the version text
            findViewById(R.id.versionTextView).setVisibility(View.INVISIBLE);
        }
    }
}
